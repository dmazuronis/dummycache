﻿using Domain.Constants;
using Domain.Dto;
using Domain.Entities;
using Domain.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace DummyCache.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class CacheController : Controller
    {
        private readonly ConcurrentDictionary<string, CacheValueEntity> _dict;
        private readonly RequestOptions _options;

        public CacheController(ConcurrentDictionary<string, CacheValueEntity> dict, IOptions<RequestOptions> options)
        {
            _dict = dict;
            _options = options.Value;
        }

        // GET: CacheController
        [HttpGet("{key}")]
        public async Task<ActionResult<CacheValueEntity>> Get(string key)
        {
            var found = _dict.TryGetValue(key, out CacheValueEntity value);
            if (found)
                return value;
            else
                return NotFound(Messages.EntryNotFound);
        }

        // POST: CacheController
        [HttpPost]
        public async Task<ActionResult<CacheValueEntity>> Create([FromBody] CacheEntryDto dto)
        {
            var duration = dto.ValidDuration ?? _options.DefaultValidDuration;
            var value = new CacheValueEntity(dto.Value, DateTime.Now.Add(duration));
            var x = _dict.TryAdd(dto.Key, value);
            if (x)
                return value;
            else
                return Conflict(Messages.AlreadyExists);
        }

        // PATCH: CacheController
        [HttpPatch]
        public async Task<ActionResult<CacheValueEntity>> Append([FromBody] CacheEntryDto dto)
        {
            var duration = dto.ValidDuration ?? _options.DefaultValidDuration;
            var value = new CacheValueEntity(dto.Value, DateTime.Now.Add(duration));
            _dict[dto.Key] = value;
            return value;
        }

        [HttpDelete("{key}")]
        public async Task<ActionResult> Delete(string key)
        {
            _dict.TryRemove(key, out _);
            return Ok();
        }
    }
}
