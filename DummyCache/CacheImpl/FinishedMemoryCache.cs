﻿using Microsoft.Extensions.Caching.Memory;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DummyCache.CacheImpl
{
    public class FinishedMemoryCache<T> : ICache<T>
    {
        private MemoryCache _cache = new MemoryCache(new MemoryCacheOptions());
        private ConcurrentDictionary<object, SemaphoreSlim> _locks = new ConcurrentDictionary<object, SemaphoreSlim>();
        public async Task Add(object key, T value)
        {
            SemaphoreSlim mylock = _locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));

            await mylock.WaitAsync();
            try
            {
                _cache.Set(key, value);
            }
            finally
            {
                mylock.Release();
            }
        }

        public async Task<T> Get(object key)
        {
            SemaphoreSlim mylock = _locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));

            await mylock.WaitAsync();
            try
            {
                return _cache.Get<T>(key);
            }
            finally
            {
                mylock.Release();
            }
        }
    }
}
