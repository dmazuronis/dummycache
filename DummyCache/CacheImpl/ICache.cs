﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DummyCache.CacheImpl
{
    public interface ICache<T>
    {
        Task Add(object key, T value);
        Task<T> Get(object key);
    }
}
