﻿using System;

namespace Domain.Exceptions
{
    public class DummyCacheException : Exception
    {
        public DummyCacheException()
        { }

        public DummyCacheException(string message)
            : base(message)
        { }

        public DummyCacheException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
