﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Options
{
    public class MaintenanceOptions
    {
        public TimeSpan PurgeOldEntriesPeriod { get; set; }
    }
}
