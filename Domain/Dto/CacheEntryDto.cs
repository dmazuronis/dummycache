﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto
{
    public class CacheEntryDto
    {
        public string Key { get; set; }

        public List<Object> Value { get; set; }

        [System.Text.Json.Serialization.JsonConverterAttribute(typeof(TimeSpanConverter))]
        public TimeSpan? ValidDuration { get; set; }
    }
}
