﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class CacheValueEntity
    {
        public CacheValueEntity(List<Object> value, DateTime expiresOn)
        {
            this.Value = value;
            this.ExpiresOn = expiresOn;
        }
        public List<Object> Value { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
