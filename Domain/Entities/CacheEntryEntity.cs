﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class CacheEntryEntity
    {
        public string Key { get; set; }
        public List<Object> Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public TimeSpan ValidDuration { get; set; }
    }
}
